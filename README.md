# Malayalam Compat

Bug reporting for Malayalam.

Malayalam Compat (inspired by Mozilla's [Web Compat](https://webcompat.com/)) is a place where you can file bug reports for any software related to Malayalam rendering, support, etc.

## Guide on how to use this repository.

* Use the [issues](https://gitlab.com/smc/malayalam-compat/-/issues) section to report bugs/issues/suggestions.
* It is ideal to first create an issue in the bug tracker of the upstream project that you're concerned about, and then link to that within the issue you create in Malayalam Compat project.
* Search for the issue before creating it!


Example:

Imagine your favorite software - XYZ - does not support Malayalam characters correctly. Here's what you can do:

A: Find the bug tracking system of XYZ. If you are unable to do so, skip to D.

B: Search if there's already an issue about Malayalam characters in that tracker. If yes, skip C and go to D. If you are unable to do so, skip to D.

C: Create an issue in XYZ's bug tracker about Malayalam characters, following XYZ's guide on how to create issue. If you rae unable to do so, skip to D.

D: Search if there's already an issue about XYZ in https://gitlab.com/smc/malayalam-compat/-/issues . If yes, skip E and go to F.

E: Create an issue in https://gitlab.com/smc/malayalam-compat/-/issues with the bug details and name of XYZ clearly mentioned.

F: If you have created an issue on XYZ's tracker and it is missing in Malayalam Compat, add it to the issue in Malayalam Compat.

G: Wait for someone to pick up the issue. Share Malayalam Compat issue tracker with others and encourage them to fix some of these issues.